package trendnxt_core_java_l1_top_gear_assignments;

class Book {

	int isbn;
	String title;
	float price;
	public Book(int isbn, String title, float price) {
		this.isbn=isbn;
		this.title=title;
		this.price=price;
	}
	public void displayDetails() {
		System.out.println("S. No. : "+this.isbn);
		System.out.println("Title  : "+this.title);
		System.out.println("Price  : "+this.price);
	}
}
class Magazine extends Book{
	private String type;
	public Magazine(int isbn, String title, String type, float price) {
		super(isbn,title,price);
		this.type= type;
	}
	public void displayMagazine() {
		System.out.println("Magazine Details :: ");
		super.displayDetails();
		System.out.println("Type  : "+this.type);
	}
}
class Novel extends Book{
	private String author;
	public Novel(int isbn, String title, String author, float price) {
		super(isbn,title,price);
		this.author= author;
	}
	public void displayNovel() {
		System.out.println("Novel Details :: ");
		super.displayDetails();
		System.out.println("Author  : "+this.author);
	}
}
public class T2Q3{
	public static void main(String[] args) {
		Magazine mag1 = new Magazine(100,"The Forbes","Business",1500);
		Novel novel = new Novel(101,"The Secret","Thriller",250);
		mag1.displayMagazine();
		novel.displayNovel();

	}
}

