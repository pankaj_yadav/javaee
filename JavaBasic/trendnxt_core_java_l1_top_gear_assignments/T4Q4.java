package trendnxt_core_java_l1_top_gear_assignments;
import java.util.HashMap;
import java.util.Scanner;
public class T4Q4 {
    

    public static void main(String[] args) {
	HashMap<String, Long> phonebook = new HashMap<String, Long>();
	phonebook.put("Pankaj", 9149962856L);
	phonebook.put("Ashna", 7508514340L);
	System.out.print("Enter the name you want to search in phonebook : ");
	
	Scanner in=new Scanner(System.in);
	String s=in.nextLine();
	long phNo=0;
	if(phonebook.containsKey(s))
	    phNo=phonebook.get(s);
	
	if(phNo==0L) {
	    System.out.println("Not Found :(");
	}
	else {
	    System.out.println("Name  : "+s+" \nPhone : "+phNo);
	}
    }

}
