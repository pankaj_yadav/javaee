package trendnxt_core_java_l1_top_gear_assignments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class T4Q3EmployeeDB {
	List<T4Q3Employee> employeeDb = new ArrayList<>();
	
	public boolean addEmployee(T4Q3Employee e) {
		return employeeDb.add(e);
	}
	
	public boolean deleteEmployee(int empId) {
		boolean isRemoved = false;
		
		Iterator<T4Q3Employee> it = employeeDb.iterator();
		
		while (it.hasNext()) {
		    T4Q3Employee emp = it.next();
			if (emp.getEmpId() == empId) {
				isRemoved = true;
				it.remove();
			}
		}
		
		return isRemoved;
	}
	
	public String showPaySlip(int empId) {
		String paySlip = "Invalid employee id";
		
		for (T4Q3Employee e : employeeDb) {
			if (e.getEmpId() == empId) {
				paySlip = "Pay slip for employee id " + empId + " is " +
						e.getEmpSalary();
			}
		}

		return paySlip;
	}
	
	public T4Q3Employee[] listAll() {
	    T4Q3Employee[] empArray = new T4Q3Employee[employeeDb.size()];
		for (int i = 0; i < employeeDb.size(); i++)
			empArray[i] = employeeDb.get(i);
		return empArray;
	}
	
}
