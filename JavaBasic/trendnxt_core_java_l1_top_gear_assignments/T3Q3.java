package trendnxt_core_java_l1_top_gear_assignments;

public class T3Q3 {

	public static int avg(int a, int b, int c, int d, int e) {
		return (a+b+c+d+e)/5;
	}
	public static void main(String[] args) {
		try {
			int a=Integer.parseInt(args[0]);
			int b=Integer.parseInt(args[1]);
			int c=Integer.parseInt(args[2]);
			int d=Integer.parseInt(args[3]);
			int e=Integer.parseInt(args[4]);
			
			System.out.println("Average = "+avg(a,b,c,d,e));
		}
		catch(ArrayIndexOutOfBoundsException a) {
			System.out.println("Entered values are less then 5.\nException Details ::\n"+a);
		}
		
		

	}

}
