package trendnxt_core_java_l1_top_gear_assignments;
import java.util.Scanner;

public class T1Q3 {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		
		System.out.print("Enter total minutes : ");
		long min=in.nextLong();
		
		int yr=0, dy=0;
		
		dy=(int)(min/(60*24));
		yr=dy/365;
		
		dy-=yr*365;
		
		System.out.print(yr+" years "+dy+" days ");

	}

}
