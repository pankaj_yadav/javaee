package trendnxt_core_java_l1_top_gear_assignments;
import java.time.format.*;

class TimeDisplay implements Runnable {
    private Thread t;
    static int num;

    public void run() {
	try {
	    for (int i = 0; i < 10; i++) {
		System.out.println("Current Time : " + java.time.LocalTime.now());
		Thread.sleep(2000);
	    }
	} catch (InterruptedException e) {
	    System.out.println(t.getName() + "Interrupted");
	}
    }
}

public class T4Q2 {
    public static void main(String args[]) {
	Thread t1 = new Thread(new TimeDisplay());
	t1.start();
    }
}
