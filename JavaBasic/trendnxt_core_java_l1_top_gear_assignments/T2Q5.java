package trendnxt_core_java_l1_top_gear_assignments;

abstract class Instrument{
	public abstract void play();
}
class Piano extends Instrument{
	public void play() {
		System.out.println("Piano is playing tan tan tan tan");
	}
}
class Flute extends Instrument{
	public void play() {
		System.out.println("Flute is playing toot toot toot toot");
	}
}
class Guitar extends Instrument{
	public void play() {
		System.out.println("Guitar is playing tin tin tin");
	}
}
public class T2Q5 {

	public static void main(String[] args) {
		Piano piano = new Piano();
		Guitar guitar = new Guitar();
		Flute flute = new Flute();
		Instrument instruments[] = {piano,guitar,piano,flute,guitar,piano,flute,flute,guitar,piano};
		for (int i =0;i < instruments.length;i++) {
			if(instruments[i] instanceof Piano) 
				piano.play();
			if(instruments[i] instanceof Guitar) 
				guitar.play();
			if(instruments[i] instanceof Flute) 
				flute.play();
		}

	}

}
