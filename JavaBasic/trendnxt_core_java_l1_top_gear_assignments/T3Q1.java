package trendnxt_core_java_l1_top_gear_assignments;

class AgeException extends Exception{
	String customMsg;
	public AgeException(String msg) {
		customMsg=msg;
	}
	public String throwExcp() {
		return (customMsg);
	}
}

public class T3Q1 {

	public static void main(String[] args) {
		String name=args[0];
		int age=Integer.parseInt(args[1]);
		
		try {
		    if (age<17||age>60) {
		    	throw new AgeException("Invalid Age :(");
		    }
		    else {
		    	System.out.println("Valid Age :)");
		    }
		}
		catch(AgeException a) {
			System.out.println(a.throwExcp()); 
		    System.out.println("Exception Type : "+a);
		    System.exit(1);
		}

	}

}
