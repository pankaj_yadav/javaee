package trendnxt_core_java_l1_top_gear_assignments;

public class T3Q2 {

	public static int calAvg(int m1, int m2) {
		return (m1+m2)/2;
	}
	
	public static void main(String[] args) {
		String name1=args[0], name2=args[4];
		try {
			int math1=Integer.parseInt(args[1]);
			int phys1=Integer.parseInt(args[2]);
			int chem1=Integer.parseInt(args[3]);
			int math2=Integer.parseInt(args[5]);
			int phys2=Integer.parseInt(args[6]);
			int chem2=Integer.parseInt(args[7]);
			System.out.println("Maths Avg="+calAvg(math1,math2)+"\nPhysics Avg="+calAvg(phys1,phys2)
								+"\nChemistry Avg="+calAvg(chem1,chem2));
			
		}
		catch(NumberFormatException a){
			System.out.println("Some of entered marks are not integer!\nException Details :: \n"+a);
		}
	}

}
