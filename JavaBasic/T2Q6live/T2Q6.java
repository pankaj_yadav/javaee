package T2Q6live;

public class T2Q6 {
	public static void main(String args[]) {
	T2Q6music.string.Veena veena = new T2Q6music.string.Veena();
	T2Q6music.wind.Saxophone saxo = new T2Q6music.wind.Saxophone();
	
	veena.play();
	saxo.play();
	
	T2Q6music.Playable playveena=veena;
	playveena.play();
	T2Q6music.Playable playsaxo=saxo;
	playsaxo.play();
	
	
	}
}
