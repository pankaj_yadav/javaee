

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class T1A2
 */
@WebServlet("/T1A2")
public class T1A2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public T1A2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    response.setContentType("text/html");
	    PrintWriter out=response.getWriter();
	    String docType =
		         "<!doctype html public \"-//w3c//dtd html 4.0 " +
		         "transitional//en\">\n";
		         
		      out.println(docType +
		         "<html>\n" +
		            "<body bgcolor = \"#f0f0f0\">\n" +
		               "<ul>\n" +
		                  "  <li><b>Name</b>: "
		                  + request.getParameter("name") + "\n" +
		                  "  <li><b>Last Name</b>: "
		                  + request.getParameter("last_name") + "\n" +
		               "</ul>\n" +
		            "</body>"+
		         "</html>"
		      );
	    
	    out.println("Enter your name : ");
	    String name=request.getParameter("name");
	    
	    
	    Date date=new Date();
	    String temp="", hour=date.toString().substring(11,13);
	    if(Integer.parseInt(hour)>=0 && Integer.parseInt(hour)<12) {
		temp+="Morning";
	    }
	    else{
		if(Integer.parseInt(hour)>=12 && Integer.parseInt(hour)<18){
		temp+="Afternoon";
		}
		else {
		    temp+="Evening";
		}
		
	    }
	    out.println("Good "+temp+", "+name);
	    
	    
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
