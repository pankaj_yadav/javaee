
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/T1A1")
public class T1A1 extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    public T1A1() {
	super();
	// TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();

	out.println("<html>");
	out.println("<head><title>Table Example</title></head>");
	out.println("<body>");
	out.println("<table border = '1'>");
	out.println("<tr>");

	for (int i = 0; i < 25; i++) {
	    
	    for (int j = 0; j < 3; j++) {
		out.println("<td>Row" + (i+1) + ", Col" + (j+1) + "</td>");
	    }
	    out.println("</tr>");
	}

	out.println("</table>");
	out.println("</body></html>");
    }

}
